#include "FastLED.h"
#include "Wire.h"
#include <stdint.h>

#define NUM_LEDS 255
#define DATA_PIN 2
#define BUFSIZE 512

#define USE_SERIAL
//#define DEBUG

CRGB leds[NUM_LEDS];

uint8_t buffer[BUFSIZE];
size_t readIdx = 0;
size_t writeIdx = 0;

enum ModeEnum { kRGB, kHSV };
ModeEnum mode = kRGB;

void eatBuf(size_t i) {
	readIdx = (readIdx + i) % BUFSIZE;
}

uint8_t getBuf(size_t i) {
	return buffer[(readIdx + i) % BUFSIZE];
}

void indexColor(uint8_t i, uint8_t& r, uint8_t& g, uint8_t& b) {
	uint8_t t = i % 36;
	r = i / 36 * 51;
	g = t / 6 * 51;
	b = (t % 6) * 51;
}

void setup() {

#ifdef USE_SERIAL
	Serial.begin(9600);
	delay(1000);
	Serial.println("Hallo");
#else
	Wire.begin(167);
	/*Wire.onReceive(handler);*/
#endif

	FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
	FastLED.setBrightness(100);
}

void loop() {
	size_t count = 0;
	size_t idx = 0;
	size_t offset = 0;
	bool gotColor = false;
	bool indexing = true;
	uint8_t r, g, b;
	while (1) {

#ifdef USE_SERIAL
		while(Serial.available()) {
			uint8_t c = Serial.read();
#else
		while(Wire.available()) {
			uint8_t c = Wire.read();
#endif
			buffer[writeIdx] = c;
			writeIdx = (writeIdx + 1) % BUFSIZE;
			if (readIdx == writeIdx) {
				Serial.println("OVERFLOW");
			}
		}

		int bufferSize = ((writeIdx < readIdx) ? (writeIdx + BUFSIZE) : writeIdx) - readIdx;

		if (count > 0) {
			if (bufferSize >= 1 && indexing) {
				uint8_t index = getBuf(0);
				if (index == 0xff) {
					indexing = false;
				} else {
#if defined(USE_SERIAL) && defined(DEBUG)
					Serial.print("Index: ");
					Serial.print(index, DEC);
					Serial.print(" ");
#endif
					indexColor(index, r, g, b);
					gotColor = true;
				}
				eatBuf(1);
			} else if (bufferSize >= 3) {
				r = getBuf(0);
				g = getBuf(1);
				b = getBuf(2);
				gotColor = true;
				eatBuf(3);
			}
			if (gotColor) {
				indexing = true;
				gotColor = false;

#if defined(USE_SERIAL) && defined(DEBUG)
				Serial.print("Setting LED to: ");
				Serial.print(r, DEC);
				Serial.print(", ");
				Serial.print(g, DEC);
				Serial.print(", ");
				Serial.print(b, DEC);
				Serial.println();
#endif
				if (mode == kRGB) {
					leds[idx + offset].setRGB(r, g, b);
				} else {
					leds[idx + offset].setHSV(r, g, b);
				}
				offset++;
				if (offset == count) {
					offset = idx = count = 0;
				}
			}
		} else if (bufferSize > 0){
			switch (getBuf(0)) {
				case 'l':
					if (bufferSize >= 3) {
						idx = getBuf(1);
						count = getBuf(2);
#if defined(USE_SERIAL) && defined(DEBUG)
						Serial.print("Setting ");
						Serial.print(count, DEC);
						Serial.print(" LEDs starting at ");
						Serial.print(idx, DEC);
						Serial.println();
#endif
						eatBuf(3);
					}
					break;
				case 'r':
#if defined(USE_SERIAL) && defined(DEBUG)
					Serial.println("Setting mode to RGB");
#endif
					mode = kRGB;
					eatBuf(1);
					break;
				case 'h':
#if defined(USE_SERIAL) && defined(DEBUG)
					Serial.println("Setting mode to HSV");
#endif
					mode = kHSV;
					eatBuf(1);
					break;
				case 'c':
#if defined(USE_SERIAL) && defined(DEBUG)
					Serial.println("Clearing LEDs");
#endif
					FastLED.clear();
					eatBuf(1);
					break;
				case 's':
#if defined(USE_SERIAL) && defined(DEBUG)
					Serial.println("Showing LEDs");
#endif
					FastLED.show();
					eatBuf(1);
					break;
				case 'b':
					if (bufferSize >= 2) {
#if defined(USE_SERIAL) && defined(DEBUG)
						Serial.print("Setting brightness to: ");
						Serial.println(getBuf(1), DEC);
#endif
						FastLED.setBrightness(getBuf(1));
						eatBuf(2);
					}
					break;
				default:
#if defined(USE_SERIAL) && defined(DEBUG)
					Serial.print("Read in junk: ");
					Serial.println(getBuf(0), DEC);
#endif
					eatBuf(1);
			}
		}
	}
}
